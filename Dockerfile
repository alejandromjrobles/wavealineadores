FROM node:14-alpine as builder

WORKDIR /app

COPY package.json ./

RUN npm install

COPY . ./

ARG ENV
ENV ENV $ENV

RUN echo "Environment: ${ENV}"
RUN npm run build-${ENV}


# Deploy en nginx:alpine

FROM nginx:1.24-alpine

COPY --from=builder /app/dist/landing-base /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
