import { AfterViewInit, Component, OnInit } from '@angular/core';

declare var KTApp: any;
declare var KTMenu: any;
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
})
export class LayoutComponent implements OnInit, AfterViewInit {
  constructor() {}

  ngOnInit(): void {}
  ngAfterViewInit(): void {}
}
