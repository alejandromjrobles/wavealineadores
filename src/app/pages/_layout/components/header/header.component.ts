import { Component, OnInit } from '@angular/core';
import { TranslationService } from 'src/app/modules/i18n/translation.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [],
})
export class HeaderComponent implements OnInit {
  languageSelected = '';
  constructor(private translationService: TranslationService) {
    this.languageSelected = translationService.getSelectedLanguage();
  }

  ngOnInit(): void {}
  onLanguageSelected(event: any) {
    if (this.translationService.getSelectedLanguage() !== event.target.value) {
      this.translationService.setLanguage(event.target.value);
      this.languageSelected = this.translationService.getSelectedLanguage();
    }
  }
}
