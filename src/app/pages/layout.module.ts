import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FooterComponent } from './_layout/components/footer/footer.component';
import { LayoutComponent } from './_layout/layout.component';
import { PagesRoutingModule } from './pages-routing.module';
import { TranslationModule } from '../modules/i18n/translation.module';
import { HeaderComponent } from './_layout/components/header/header.component';

@NgModule({
  declarations: [FooterComponent, LayoutComponent, HeaderComponent],
  imports: [CommonModule, PagesRoutingModule, TranslationModule],
  bootstrap: [LayoutComponent],
})
export class LayoutModule {}
