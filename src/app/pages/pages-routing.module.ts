import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './_layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('../modules/landing/landing.module').then(
            (m) => m.LandingModule
          ),
      },
      
      {
        path: '',
        redirectTo: '',
        pathMatch: 'full',
      },

      {
        path: 'error',
        loadChildren: () =>
          import('../modules/errors/errors.module').then((m) => m.ErrorsModule),
      },
      { path: '**', redirectTo: 'error/404' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
