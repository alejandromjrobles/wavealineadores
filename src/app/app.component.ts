import { Component } from '@angular/core';
import { TranslationService } from './modules/i18n/translation.service';

import { locale as enLang } from './modules/i18n/vocabs/en';
import { locale as esLang } from './modules/i18n/vocabs/es';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  constructor(private translationService: TranslationService) {
    this.translationService.loadTranslations(enLang, esLang);
  }
}
