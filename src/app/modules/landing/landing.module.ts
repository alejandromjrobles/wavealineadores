import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './landing.component';
import { HeroComponent } from './_partials/hero/hero.component';
import { FeaturesComponent } from './_partials/features/features.component';
import { FAQComponent } from './_partials/faq/faq.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TranslationModule } from '../i18n/translation.module';
import { TeamComponent } from './_partials/team/team.component';
import { ItemsComponent } from './_partials/items/items.component';

@NgModule({
  declarations: [
    LandingComponent,
    HeroComponent,
    FeaturesComponent,
    FAQComponent,
    TeamComponent,
    ItemsComponent,
  ],
  imports: [
    CommonModule,
    LandingRoutingModule,
    SharedModule,
    TranslationModule,
  ],
  bootstrap: [LandingComponent],
})
export class LandingModule {}
