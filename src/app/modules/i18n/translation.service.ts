// Localization is based on '@ngx-translate/core';
// Please be familiar with official documentations first => https://github.com/ngx-translate/core

import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import Utils from 'src/app/core/util/utils';

export interface Locale {
  lang: string;
  data: any;
}

const LOCALIZATION_LOCAL_STORAGE_KEY = 'LANG_KEY';
const dateNow = new Date();
dateNow.setDate(100);

@Injectable({
  providedIn: 'root',
})
export class TranslationService {
  // Private properties
  private langIds: any = [];

  constructor(
    private translate: TranslateService,
    private cookieService: CookieService
  ) {}

  loadTranslations(...args: Locale[]): void {
    const locales = [...args];
    locales.forEach((locale) => {
      this.translate.setTranslation(locale.lang, locale.data, true);
      this.langIds.push(locale.lang);
    });
    // add new languages to the list
    this.translate.addLangs(this.langIds);
    const langFromBrowser = Utils.getLangFromBrowser();
    if (!!this.getSelectedLanguage()) {
      this.setLanguage(this.getSelectedLanguage());
    } else {
      this.setLanguage('en'); //the language is added by default in English
      if (
        this.translate.getLangs().find((v) => langFromBrowser.includes(v)) !==
        undefined
      ) {
        this.setLanguage(langFromBrowser);
      }
    }
  }

  setLanguage(lang: string) {
    if (lang) {
      this.translate.use(this.translate.getDefaultLang());
      this.translate.use(lang);
      this.cookieService.delete(LOCALIZATION_LOCAL_STORAGE_KEY, '/');
      this.cookieService.set(
        LOCALIZATION_LOCAL_STORAGE_KEY,
        lang,
        dateNow.setDate(1)
      );
    }
  }

  /**
   * Returns selected language
   */
  getSelectedLanguage(): any {
    return (
      this.cookieService.get(LOCALIZATION_LOCAL_STORAGE_KEY) ||
      this.translate.getDefaultLang()
    );
  }
}
