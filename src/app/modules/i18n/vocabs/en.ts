// USA
export const locale = {
  lang: 'en',
  data: {
    TRANSLATOR: {
      SELECT: 'Select your language',
      ENGLISH: 'english',
      SPANISH: 'spanish',
    },
    HEADER: {
      FUNCTION: '¿Cómo funciona?',
      MONETIZATION: 'Monetización',
      BUSINESS: 'Empresas',
    },
    FOOTER: {
      PRESSKIT: 'Press Kit',
      POLICY: 'Política de privacidad',
      TERMS: 'Términos y condiciones',
      JOBS: 'Trabaja con nosotros',
      BY: 'Derechos reservados by AMLA S.A.S. Copyright',
      TITLE: '#1 Aplicación de voces para compartir emociones.',
      SECOND_TITLE:
        'Ayudamos a personas compartir o percibir emociones solo con la voz.',
    },
    TERMS: {
      TITLE: 'TÉRMINOS Y CONDICIONES',
      PARAGRAPH_1:
        'En Whisfy Software, S.A.S. ("Whisfy", "nosotros", "nos" o "nuestro") hemos creado esta política de privacidad (esta "Política de privacidad") porque queremos que sepa cómo es la información que nos proporciona usado y compartido. Esta Política de privacidad se relaciona con la recopilación de información y las prácticas de uso de Whisfy en relación con nuestro Servicio, que se pone a su disposición a través de nuestro sitio web (incluida la web móvil) ubicado en https://Whisfy.io , nuestra aplicación y nuestra plataforma (que puede se puede acceder a través del sitio web o la aplicación).',
      PARAGRAPH_2:
        "Brindamos acceso y uso de nuestro Servicio sujeto a los siguientes términos de servicio (los 'Términos de servicio'), que entran en vigencia a partir de la fecha indicada anteriormente. Al utilizar el Servicio, usted reconoce que ha leído, entendido y acepta estar legalmente obligado por los términos y condiciones de estos Términos de servicio y los términos y condiciones de nuestra Política de privacidad, que se incorpora aquí como referencia. Tómese el tiempo para revisar nuestra Política de privacidad. Si no acepta alguno de los términos de estos Términos de servicio o la Política de privacidad, no utilice nuestro Servicio.",
      PARAGRAPH_3:
        'Podemos cambiar estos Términos de servicio de vez en cuando, y publicaremos cualquier cambio en el sitio web o le notificaremos por correo electrónico, a nuestra opción, tan pronto como dichos cambios entren en vigencia. Si está utilizando el Servicio a modo de prueba gratuita, al continuar usando el Servicio después de que realicemos dichos cambios en los Términos de servicio, se considera que ha aceptado dichos cambios. Si ha comprado una suscripción al Servicio, los cambios no entrarán en vigor hasta que se renueve su suscripción al Servicio. Consulte estos Términos de servicio con regularidad.',
      SUB_TITLE_1: '<b>Acceso y uso</b>',
      PARAGRAPH_4:
        "Sujeto a los términos y condiciones de este Acuerdo, Whisfy le proporcionará acceso y uso del Servicio a usted oa la entidad que usted tiene autoridad para vincular a este Acuerdo ('Suscriptor'). También proporcionaremos acceso y uso del Servicio a cualquier empleado del Suscriptor u otra persona autorizada por el Suscriptor (cada uno, un 'Usuario autorizado'). El Suscriptor reconoce que cada Usuario autorizado debe aceptar estos Términos de servicio y la Política de privacidad antes de su uso y que el Suscriptor será responsable de garantizar el cumplimiento por parte de cada Usuario autorizado de estos Términos de servicio y de cualquier incumplimiento de estos Términos de servicio por parte de cualquier Autorizado. Usuario.",
      PARAGRAPH_5:
        "El Suscriptor es el único responsable de cualquier dato o contenido cargado o almacenado en el Servicio por el Suscriptor o cualquier Usuario Autorizado ('Datos del Suscriptor'). En ningún caso Whisfy será responsable del uso o mal uso de los Datos del Suscriptor por parte del Suscriptor o cualquier Usuario Autorizado u otro tercero. El suscriptor garantiza y declara que posee o tiene derecho a proporcionar todos los datos del suscriptor. Por la presente, el Suscriptor otorga a Whisfy una licencia no exclusiva, transferible y libre de regalías para usar los Datos del Suscriptor para proporcionar el Servicio y como se describe en este documento o en nuestra Política de Privacidad. Como se indica en la Política de privacidad, tomamos las medidas razonables para proteger los Datos del suscriptor contra pérdida, mal uso y acceso no autorizado, divulgación, alteración o destrucción. Para evitar dudas.",
      PARAGRAPH_6:
        "Todo derecho, título e interés en y para el Servicio, el Sitio web, la Aplicación y cualquier información, datos, software, gráficos y características interactivas contenidas en el mismo, incluidas todas las modificaciones, mejoras, adaptaciones, mejoras o traducciones realizadas al mismo. y todos los derechos de propiedad en cualquiera de los anteriores (colectivamente, 'Propiedad de whisfy'), serán y seguirán siendo propiedad única y exclusiva de whisfy.",
      PARAGRAPH_7:
        'El Suscriptor no permitirá, y no permitirá que ningún Usuario Autorizado u otro tercero: (i) permita que nadie que no sea un Usuario Autorizado acceda o use el Servicio; (ii) usar el Servicio de cualquier manera que no esté expresamente permitida por este Acuerdo, incluyendo, sin limitación, ingeniería inversa, modificación, copia, distribución o sublicencia del Servicio, o introducción en el Servicio de cualquier software, virus o código; o (iii) utilizar el Servicio en violación de cualquier ley o reglamento aplicable.',
      SUB_TITLE_2: '<b>Publicidad</b>',
      PARAGRAPH_8:
        'Cada suscriptor puede identificarse como suscriptor de los servicios con fines promocionales y de marketing. El suscriptor otorga a Whisfy una licencia no exclusiva, no transferible, no sublicenciable y libre de regalías para usar y reproducir el nombre, los logotipos y las marcas comerciales del Suscriptor con fines promocionales y de marketing, incluso en las listas de clientes, publicidad y sitio web de Whisfy. El suscriptor puede optar por no participar en las disposiciones de esta sección enviando una solicitud por correo electrónico a support@Whisfy.io . En otras palabras, estamos orgullosos de tener la calidad de clientes que tenemos. Si surge, ¡podemos mencionarlo!',
      SUB_TITLE_3: '<b>Duración y Terminación</b>',
      PARAGRAPH_9:
        'Whisfy puede suspender o cancelar su acceso y uso del Servicio, en su totalidad o en parte, en cualquier momento y por cualquier motivo; siempre que, sin embargo, si ha comprado una suscripción para el Servicio, el derecho de Whisfy a suspender o cancelar su acceso y uso del Servicio se limitará a los casos en los que no haya pagado las tarifas de suscripción aplicables o haya incumplido estos Términos. del Servicio, y no han subsanado dicho incumplimiento de pago u otro incumplimiento dentro de los 10 días hábiles posteriores a la recepción de un aviso por escrito de dicho incumplimiento de pago u otro incumplimiento de Whisfy (y siempre que Whisfy pueda suspender su acceso y uso del Servicio inmediatamente sin previo aviso). en el caso de que Whisfy determine razonablemente que su cuenta puede causar daño potencial a Whisfy oa terceros). Puede cancelar su cuenta en cualquier momento si nos notifica; siempre que, sin embargo, si ha comprado una suscripción para el Servicio, su derecho a cancelar su cuenta antes de pagar el monto total de las tarifas por el período de suscripción al que se ha comprometido se limitará a los casos en los que Whisfy haya incumplido estos Términos de servicio. , y no ha subsanado dicha infracción dentro de los 10 días hábiles posteriores a la recepción de su notificación por escrito de dicha infracción. En caso de suspensión o rescisión (excepto en los casos en que Whisfy bloquea su cuenta debido a actividades fraudulentas u otro daño potencial a Whisfy o terceros), Whisfy le proporcionará acceso a sus Datos de suscriptor durante al menos 30 días después de dicha rescisión. Es su responsabilidad mantener copias de seguridad de los datos del suscriptor. sin embargo, que si ha comprado una suscripción para el Servicio, su derecho a cancelar su cuenta antes de pagar el monto total de las tarifas por el período de suscripción al que se comprometió se limitará a los casos en que Whisfy haya incumplido estos Términos de servicio, y no ha subsanado dicha infracción dentro de los 10 días hábiles posteriores a la recepción de su notificación por escrito de dicha infracción. En caso de suspensión o rescisión (excepto en los casos en que Whisfy bloquea su cuenta debido a actividades fraudulentas u otro daño potencial a Whisfy o terceros), Whisfy le proporcionará acceso a sus Datos de suscriptor durante al menos 30 días después de dicha rescisión. Es su responsabilidad mantener copias de seguridad de los datos del suscriptor. sin embargo, que si ha comprado una suscripción para el Servicio, su derecho a cancelar su cuenta antes de pagar el monto total de las tarifas por el período de suscripción al que se comprometió se limitará a los casos en que Whisfy haya incumplido estos Términos de servicio, y no ha subsanado dicha infracción dentro de los 10 días hábiles posteriores a la recepción de su notificación por escrito de dicha infracción. En caso de suspensión o rescisión (excepto en los casos en que Whisfy bloquea su cuenta debido a actividades fraudulentas u otro daño potencial a Whisfy o terceros), Whisfy le proporcionará acceso a sus Datos de suscriptor durante al menos 30 días después de dicha rescisión. Es su responsabilidad mantener copias de seguridad de los datos del suscriptor. su derecho a cancelar su cuenta antes de pagar el monto total de las tarifas para el período de suscripción al que se comprometió se limitará a los casos en que Whisfy haya violado estos Términos de servicio y no haya subsanado dicho incumplimiento dentro de los 10 días hábiles posteriores a la recepción del aviso por escrito de tal incumplimiento por su parte. En caso de suspensión o rescisión (excepto en los casos en que Whisfy bloquea su cuenta debido a actividades fraudulentas u otro daño potencial a Whisfy o terceros), Whisfy le proporcionará acceso a sus Datos de suscriptor durante al menos 30 días después de dicha rescisión. Es su responsabilidad mantener copias de seguridad de los datos del suscriptor. su derecho a cancelar su cuenta antes de pagar el monto total de las tarifas para el período de suscripción al que se comprometió se limitará a los casos en que Whisfy haya violado estos Términos de servicio y no haya subsanado dicho incumplimiento dentro de los 10 días hábiles posteriores a la recepción del aviso por escrito de tal incumplimiento por su parte. En caso de suspensión o rescisión (excepto en los casos en que Whisfy bloquea su cuenta debido a actividades fraudulentas u otro daño potencial a Whisfy o terceros), Whisfy le proporcionará acceso a sus Datos de suscriptor durante al menos 30 días después de dicha rescisión. Es su responsabilidad mantener copias de seguridad de los datos del suscriptor. y no ha subsanado dicha infracción dentro de los 10 días hábiles posteriores a la recepción de su notificación por escrito de dicha infracción. En caso de suspensión o rescisión (excepto en los casos en que Whisfy bloquea su cuenta debido a actividades fraudulentas u otro daño potencial a Whisfy o terceros), Whisfy le proporcionará acceso a sus Datos de suscriptor durante al menos 30 días después de dicha rescisión. Es su responsabilidad mantener copias de seguridad de los datos del suscriptor. y no ha subsanado dicha infracción dentro de los 10 días hábiles posteriores a la recepción de su notificación por escrito de dicha infracción. En caso de suspensión o rescisión (excepto en los casos en que Whisfy bloquea su cuenta debido a actividades fraudulentas u otro daño potencial a Whisfy o terceros), Whisfy le proporcionará acceso a sus Datos de suscriptor durante al menos 30 días después de dicha rescisión. Es su responsabilidad mantener copias de seguridad de los datos del suscriptor. Whisfy puede rescindir este acuerdo mediante notificación por escrito si Whisfy determina, a su exclusivo y absoluto criterio, que el Cliente ha participado o permitido un comportamiento que Whisfy considera inmoral, racista o discriminatorio por motivos de raza, origen étnico, nacionalidad, casta. , orientación sexual, género, identidad de género, afiliación religiosa, edad, discapacidad o enfermedad grave.',

      SUB_TITLE_4: '<b>EXENCIONES DE RESPONSABILIDAD; GARANTÍA LIMITADA</b>',
      PARAGRAPH_10:
        "LA PROPIEDAD DE WHISFY SE PROPORCIONA 'TAL CUAL' Y 'SEGÚN ESTÉ DISPONIBLE', Y WHISFY NO OFRECE NINGUNA GARANTÍA CON RESPECTO A LA MISMA O DE OTRA MANERA EN RELACIÓN CON ESTOS TÉRMINOS DE SERVICIO Y POR LA PRESENTE RENUNCIA A CUALQUIER GARANTÍA EXPRESA, IMPLÍCITA O LEGAL INCLUYE, SIN LIMITACIÓN, CUALQUIER GARANTÍA DE COMERCIABILIDAD, APTITUD PARA UN PROPÓSITO PARTICULAR, DISPONIBILIDAD, FUNCIONAMIENTO SIN ERRORES O ININTERRUMPIDO Y CUALQUIER GARANTÍA QUE SURJA DE UN CURSO DE NEGOCIACIÓN, CURSO DE RENDIMIENTO O USO DE COMERCIO. EN LA MEDIDA EN QUE WHISFY, COMO ASUNTO DE LA LEY APLICABLE, NO RENUNCIE A CUALQUIER GARANTÍA IMPLÍCITA, EL ALCANCE Y DURACIÓN DE DICHA GARANTÍA SERÁN LOS MÍNIMOS PERMITIDOS BAJO DICHA LEY.",
    },
    PRIVACY: {
      TITLE: 'POLÍTICAS DE PRIVACIDAD',
      PARAGRAPH_1:
        "En Whisfy Software, S.A.S. . ('Whisfy', 'nosotros', 'nos' o 'nuestro') hemos creado esta política de privacidad (esta 'Política de privacidad') porque queremos que sepa cómo es la información que nos proporciona usado y compartido. Esta Política de privacidad se relaciona con la recopilación de información y las prácticas de uso de Whisfy en relación con nuestro Servicio, que se pone a su disposición a través de nuestro sitio web (incluida la web móvil) ubicado en https://Whisfy.io , nuestra aplicación y nuestra plataforma (que puede se puede acceder a través del sitio web o la aplicación).",
      PARAGRAPH_2:
        'Al visitar nuestro sitio web o utilizar nuestra plataforma, acepta los términos de esta política de privacidad y los términos de servicio que la acompañan.',
      PARAGRAPH_3:
        'Los términos en mayúscula no definidos en esta Política de privacidad tendrán el significado establecido en nuestros Términos de servicio.',
      SUB_TITLE_1: '<b>Alcance de esta política</b>',
      PARAGRAPH_4:
        'El sitio web y la plataforma son operados por Whisfy en los Estados Unidos (EE. UU.) Y cumplen con la ley de EE. UU. Si se encuentra fuera de los EE. UU., Tenga en cuenta que cualquier información que nos proporcione puede ser transferida, procesada, mantenida y utilizada en computadoras, servidores y sistemas ubicados fuera de su país de origen, donde las leyes de protección de datos pueden no sea tan protector como los de su jurisdicción.',
      SUB_TITLE_2: '<b>Individuos del EEE</b>',
      PARAGRAPH_5:
        "Las personas físicas ubicadas en el Espacio Económico Europeo ('EEE') deben revisar el aviso de privacidad del Reglamento General de Protección de Datos ('GDPR') para conocer sus derechos específicos, recursos y otra información relacionada con el Escudo de Privacidad / GDPR aquí.",
      SUB_TITLE_3: '<b>Información que recopilamos y cómo la usamos</b>',
      PARAGRAPH_6:
        '# Información del contacto # Información del pago # De tu actividad # De Cookies.',

      SUB_TITLE_4: '<b>Análisis de terceros</b>',
      PARAGRAPH_7:
        'Utilizamos uno o más servicios de análisis de terceros (como Google Analytics) para evaluar su uso del sitio web y la plataforma, compilar informes sobre la actividad (en función de su recopilación de direcciones IP, proveedor de servicios de Internet, tipo de navegador, sistema operativo e idioma, páginas de referencia y de salida y URL, datos y tiempo, cantidad de tiempo dedicado a páginas particulares, qué secciones del sitio web y / o la plataforma visita, número de enlaces en los que hace clic mientras está en el sitio web y / o la plataforma, búsqueda términos y otros datos de uso similares) y analizar métricas de rendimiento. Estos terceros utilizan cookies y otras tecnologías para ayudar a analizar y proporcionarnos los datos. Al visitar el sitio web y acceder y utilizar los servicios y / o la plataforma, usted da su consentimiento para el procesamiento de sus datos por parte de estos proveedores de análisis de la manera y para los fines establecidos en esta Política de privacidad. Para obtener más información sobre estos terceros, incluido cómo excluirse de cierta recopilación de datos, visite los sitios a continuación. Tenga en cuenta que si opta por no recibir cualquier servicio, es posible que no pueda utilizar la funcionalidad completa de los Sitios web, el Servicio o la Plataforma.',
      SUB_TITLE_5: '<b>Información agregada</b>',

      PARAGRAPH_8:
        'En un esfuerzo continuo por comprender mejor a nuestros usuarios y el sitio web, podríamos analizar su información en forma agregada para operar, mantener, administrar y mejorar el sitio web. Esta información agregada no le identifica personalmente. Podemos compartir estos datos agregados con nuestros afiliados, agentes y socios comerciales. También podemos divulgar estadísticas de usuarios agregadas para describir nuestros productos y servicios a socios comerciales actuales y potenciales y a otros terceros para otros fines legales.',
      SUB_TITLE_6: '<b>Divulgación a las autoridades públicas</b>',

      PARAGRAPH_9:
        'Estamos obligados a divulgar información personal en respuesta a solicitudes legales de las autoridades públicas, incluso con el fin de cumplir con los requisitos de seguridad nacional o de aplicación de la ley. También podemos divulgar información personal a otros terceros cuando nos vean obligados a hacerlo por las autoridades gubernamentales o cuando así lo exijan las leyes o reglamentos, incluidos, entre otros, en respuesta a órdenes judiciales y citaciones.',
      SUB_TITLE_7: '<b>Cambios a esta política de privacidad</b>',

      PARAGRAPH_10:
        'Esta Política de privacidad entra en vigencia a partir de la fecha indicada en la parte superior de esta Política de privacidad. Podemos cambiar esta Política de privacidad de vez en cuando, y publicaremos cualquier cambio en el sitio web o le notificaremos por correo electrónico, a nuestra opción, tan pronto como entren en vigencia. Al utilizar el sitio web, la aplicación o el servicio después de realizar dichos cambios en esta política de privacidad, se considera que ha aceptado dichos cambios. Consulte esta Política de privacidad con regularidad.',
      SUB_TITLE_8: '<b>Retención de información personal.</b>',

      PARAGRAPH_11:
        'Retendremos su información personal en una forma que lo identifique solo durante el tiempo que sirva para los fines para los que se recopiló inicialmente como se indica en esta Política de privacidad, posteriormente autorizado o según lo permita la ley aplicable.',
      SUB_TITLE_9: '<b>Cómo protegemos su información</b>',

      PARAGRAPH_12:
        'Nos tomamos muy en serio la seguridad y la privacidad de la información personal que recopila de conformidad con esta Política de privacidad. En consecuencia, implementaremos medidas de seguridad razonables y apropiadas para proteger su información personal contra pérdida, uso indebido y acceso no autorizado, divulgación, alteración y destrucción, teniendo en cuenta los riesgos involucrados en el procesamiento y la naturaleza de dichos datos, y cumpliremos con las leyes aplicables y regulaciones.',
    },
    JOBS: {
      TITLE: '¡En whisfy te estamos buscando!',
      SECOND_TITLE:
        'Si eres inconformista y buscas desarrollarte profesionalmente en un equipo comprometido y divertido -todo hay que decirlo 😏-, whisfy es tu sitio. Actualmente tenemos estas vacantes abiertas. Si hay alguna que encaje contigo, estaremos encantados de conocerte 🙌',
      OFFER: 'Ofertas de trabajo',
      OFFERS: {
        JOBS_1: {
          CITY: 'LATAM',
          JOB: 'Manager de Influencer Marketing LATAM',
          CATEGORY: 'Marketing & Comunicación',
        },
        JOBS_2: {
          CITY: 'Argentina',
          JOB: '¡Buscamos talento! Mándanos tu CV 🚀',
          CATEGORY: 'General',
        },
      },
    },
    LANDING: {
      CALL_TO_ACTION: {
        TITLE: '¡Te damos la bienvenida a tu comunidad!',
        SECOND_TITLE: 'Súmate a tus compañeros y amigos en whisfy.',
      },
      APP: {
        TITLE: 'CAPTURAS',
        SECOND_TITLE: 'Diseño <span>& flujo de</span> whisfy',
      },
      MONETIZATION: {
        TITLE: 'Generá ingresos con tu tiempo y contenido',
        FIRST_STEP: 'Crear una cuenta',
        SECOND_STEP: 'Generar contenido',
        THIRD_STEP: 'Genera ingresos',
        SECOND_TITLE:
          'En whisfy valoramos mucho tu tiempo, es por ello que monetizamos todas las publicidades y contenidos habilitados que consumas.',
      },
      FEATURES: {
        TITLE: 'Creá tu cuenta ahora',
        SECOND_TITLE:
          'Una aplicación con gran experiencia, <span>interactiva y moderna.</span>',
        ITEMS: {
          ITEM_1: {
            TITLE: 'Personas no videntes',
            DETAILS: 'En whisfy predomina la voz y el sonido.',
          },
          ITEM_2: {
            TITLE: 'Amigos',
            DETAILS:
              'Cualquiera puede interactuar con sus amistades en whisfy.',
          },
          ITEM_3: {
            TITLE: 'Influencers',
            DETAILS: 'Los llamados creadores de contenido podrán.',
          },
          ITEM_4: {
            TITLE: 'Aficionados',
            DETAILS: 'Quien quiera convertirse en creador de contenidos.',
          },
          ITEM_5: {
            TITLE: 'Podcasters',
            DETAILS: 'Podrás subirlos como post completos a tu perfil.',
          },
          ITEM_6: {
            TITLE: 'Personas no videntes',
            DETAILS: 'En whisfy predomina la voz y el sonido.',
          },
        },
      },
      HERO: {
        TITLE: 'Whisfy <sup>1.0</sup>',
        SECOND_TITLE:
          'Compartí momentos, experiencias o emociones sólo con tu voz.',
      },

      SHARED: {
        DOWNLOAD: 'Descargá',
        BUTTONS: {
          GOOGLE_PAY: 'Google play',
          APP_STORE: 'App Store',
        },
        SECOND_TITLE:
          'Compartí momentos, experiencias o emociones sólo con tu voz.',
      },
    },
  },
};
