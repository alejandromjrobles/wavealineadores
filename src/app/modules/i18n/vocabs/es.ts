// USA
export const locale = {
  lang: 'es',
  data: {
    TRANSLATOR: {
      SELECT: 'Select your language',
      ENGLISH: 'ingles',
      SPANISH: 'español',
    },
    HEADER: {
      FUNCTION: '¿Cómo funciona?',
      MONETIZATION: 'Monetización',
      BUSINESS: 'Empresas',
    },
    FOOTER: {
      BY: 'Derechos reservados by Osteoblast  S.A.S. Copyright',
      TITLE: 'Contribuimos en tu sonrisa.',
    },

    LANDING: {
      CALL_TO_ACTION: {
        TITLE: '¡Te damos la bienvenida a tu comunidad!',
        SECOND_TITLE: 'Súmate a tus compañeros y amigos en whisfy.',
      },
      APP: {
        TITLE: 'CAPTURAS',
        SECOND_TITLE: 'Diseño <span>& flujo de</span> whisfy',
      },
      MONETIZATION: {
        TITLE: 'Generá ingresos con tu tiempo y contenido',
        FIRST_STEP: 'Crear una cuenta',
        SECOND_STEP: 'Generar contenido',
        THIRD_STEP: 'Genera ingresos',
        SECOND_TITLE:
          'En whisfy valoramos mucho tu tiempo, es por ello que monetizamos todas las publicidades y contenidos habilitados que consumas.',
      },
      FEATURES: {
        TITLE: 'Creá tu cuenta ahora',
        SECOND_TITLE:
          'Una aplicación con gran experiencia, <span>interactiva y moderna.</span>',
        ITEMS: {
          ITEM_1: {
            TITLE: 'Personas no videntes',
            DETAILS: 'En whisfy predomina la voz y el sonido.',
          },
          ITEM_2: {
            TITLE: 'Amigos',
            DETAILS:
              'Cualquiera puede interactuar con sus amistades en whisfy.',
          },
          ITEM_3: {
            TITLE: 'Influencers',
            DETAILS: 'Los llamados creadores de contenido podrán.',
          },
          ITEM_4: {
            TITLE: 'Aficionados',
            DETAILS: 'Quien quiera convertirse en creador de contenidos.',
          },
          ITEM_5: {
            TITLE: 'Podcasters',
            DETAILS: 'Podrás subirlos como post completos a tu perfil.',
          },
          ITEM_6: {
            TITLE: 'Personas no videntes',
            DETAILS: 'En whisfy predomina la voz y el sonido.',
          },
        },
      },
      HERO: {
        TITLE: 'Whisfy <sup>1.0</sup>',
        SECOND_TITLE:
          'Compartí momentos, experiencias o emociones sólo con tu voz.',
      },

      SHARED: {
        DOWNLOAD: 'Descargá',
        BUTTONS: {
          GOOGLE_PAY: 'Google play',
          APP_STORE: 'App Store',
        },
        SECOND_TITLE:
          'Compartí momentos, experiencias o emociones sólo con tu voz.',
      },
    },
  },
};
