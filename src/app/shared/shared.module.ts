import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BtnGplayComponent } from './button/btn-gplay/btn-gplay.component';
import { BtnApplestoreComponent } from './button/btn-applestore/btn-applestore.component';
import { TranslationModule } from '../modules/i18n/translation.module';

@NgModule({
  declarations: [BtnGplayComponent, BtnApplestoreComponent],
  imports: [CommonModule, TranslationModule],
  exports: [BtnGplayComponent, BtnApplestoreComponent],
})
export class SharedModule {}
