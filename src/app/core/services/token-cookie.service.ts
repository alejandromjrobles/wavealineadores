import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const dateNow = new Date();
dateNow.setHours(dateNow.getHours() + 1);

@Injectable({
  providedIn: 'root',
})
export class TokenCookieService {
  constructor(private router: Router, private cookieService: CookieService) {}
  signOut(): void {
    this.cookieService.delete(TOKEN_KEY, '/');
    this.cookieService.delete(USER_KEY, '/');
    this.cookieService.delete(TOKEN_KEY, '/app');
    this.cookieService.delete(USER_KEY, '/app');
  }

  public saveToken(token: string): void {
    this.cookieService.delete(TOKEN_KEY);
    this.cookieService.set(TOKEN_KEY, token, dateNow);
  }

  public getToken(): string | null {
    return this.cookieService.get(TOKEN_KEY);
  }

  public saveUser(user: any): void {
    this.cookieService.delete(USER_KEY);
    this.cookieService.set(USER_KEY, JSON.stringify(user), dateNow);
  }

  public getUser(): any {
    const user = this.cookieService.get(USER_KEY);
    if (user) {
      return JSON.parse(user);
    }

    return {};
  }
}
